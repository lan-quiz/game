import React from 'react';
import { Provider } from 'react-redux';

import Question from './screens/question/Question';
import store from './store/Store';

import './App.css';
import Categories from './screens/categories/Categories';

function App() {
  return (
    <Provider store={store}>
      <div className="App">
        <Question/>
      </div>
    </Provider>
  );
}

export default App;
