import React, { useState, useEffect } from 'react';
import './style.css';
import { useDispatch } from 'react-redux';

const Countdown = () => {
  const dispatch = useDispatch();
  const [seconds, setSeconds] = useState(3);

  if (seconds > 0) {
    setTimeout(() => setSeconds(seconds - 1), 1000);
  } else {
    dispatch({type: 'NEXT_QUESTION_PHASE'});
  }

  return (
    <div className='countdown'>
      {seconds}
    </div>
  );
}

export default Countdown;
