import React, {useEffect} from 'react';
import { useSelector } from 'react-redux';
import './style.css';

const QuestionContent = () => {
  const audioUrl = useSelector(state => state.currentQuestion.audio);
  const text = useSelector(state => state.currentQuestion.text);

  useEffect(() => {
    setTimeout(() => document.getElementById('audio').play(), 10);
  }, []);

  return (
    <div className='question-content'>
      <div className='question-content__text'>{text}</div>
      <audio controls style={{ display: 'none' }} id="audio">
        <source src={audioUrl} type="audio/mpeg" />
      </audio>
      <script>
      </script>
    </div>
  )
}

export default QuestionContent;
