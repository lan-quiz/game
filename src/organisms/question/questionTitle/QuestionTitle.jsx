import React from 'react';
import './style.css';
import { useSelector, useDispatch } from 'react-redux';

const QuestionTitle = () => {
  const title = useSelector(state => state.currentQuestion.title);
  const dispatch = useDispatch();
  setTimeout(() => dispatch({type: 'NEXT_QUESTION_PHASE'}), 3000);
  return (
    <div className='question-title'>
      {title}
    </div>
  );
}

export default QuestionTitle;
