const phases = [ 'title', 'countdown', 'question' ];

const initialState = {
  phase: 'title',
  title: 'Дроп зе майк нига бич 😎',
  text: 'Играет демонстрационный семпл',
  audio: 'https://file-examples.com/wp-content/uploads/2017/11/file_example_MP3_700KB.mp3',
};

const currentQuestion = (state = initialState, action) => {
  switch(action.type) {
    case 'NEXT_QUESTION_PHASE': {
      const currentPhaseIndex = phases.indexOf(state.phase);
      return {
        ...state,
        phase: phases[currentPhaseIndex + 1]
      }
    }
    default:
      return state;
  }
};

export default currentQuestion;
