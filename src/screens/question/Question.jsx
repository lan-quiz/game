import React from 'react';
import { useSelector } from 'react-redux';
import QuestionTitle from '../../organisms/question/questionTitle/QuestionTitle';
import Countdown from '../../organisms/question/countdown/Countdown';
import QuestionContent from '../../organisms/question/questionContent/QuestionContent';
import './style.css';


const Question = () => {
  const currentPhase = useSelector((state) => state.currentQuestion.phase);

  const phases = {
    'title': <QuestionTitle />,
    'countdown': <Countdown />,
    'question': <QuestionContent />
  };

  return (
    <div className='question'>
      {phases[currentPhase]}
    </div>
  );
}

export default Question;
