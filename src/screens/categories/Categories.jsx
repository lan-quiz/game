import React from 'react';
import './style.css';
import { useSelector } from 'react-redux';

const Categories = () => {
  const buildQuestionCells = (questions) => questions.map(
    (question) => (
      <td className='categories__cell categories__cell_cost'>{question.cost}</td>
    )
  );

  const buildCategoryRows = (categories) => categories.map(
    (category) => (
      <tr className='categories__row'>
        <td className='categories__cell'>{category.title}</td>
        {buildQuestionCells(category.questions)}
      </tr>
    )
  );

  const categories = useSelector(state => state.categories.categories);

  return (
    <table className='categories'>
      {buildCategoryRows(categories)}
    </table>
  )
};

export default Categories;
